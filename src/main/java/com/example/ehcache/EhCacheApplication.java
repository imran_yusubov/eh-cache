package com.example.ehcache;

import com.example.ehcache.domain.Country;
import com.example.ehcache.repo.CountryRepository;
import com.example.ehcache.service.CountryService;
import com.example.ehcache.service.Service1;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class EhCacheApplication implements CommandLineRunner {

    private final CountryService countryService;
    private final Service1 service1;
    private final CountryRepository countryRepository;

    public static void main(String[] args) {
        SpringApplication.run(EhCacheApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Country country = new Country();
        country.setName("Azerbaijan");
        countryRepository.save(country);
        service1.getCountry();
        countryService.update(1L, "RUSSIA");
        service1.getCountry();
    }
}
