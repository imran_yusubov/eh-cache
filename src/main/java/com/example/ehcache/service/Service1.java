package com.example.ehcache.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class Service1 {

    private final CountryService countryService;

   // @Transactional
    public void getCountry() {
        System.out.println("1->" + countryService.getCountry(1l));

        System.out.println("2->" + countryService.getCountry(1l));

        System.out.println("3->" + countryService.getCountry(1l));
    }
}
