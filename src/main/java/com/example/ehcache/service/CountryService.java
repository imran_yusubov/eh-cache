package com.example.ehcache.service;

import com.example.ehcache.domain.Country;
import com.example.ehcache.repo.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CountryService {

    private final CountryRepository countryRepository;

    @Cacheable(cacheNames = "countryCache", key = "#id")
    public Country getCountry(long id) {
        return countryRepository.findById(1L).get();
    }

    @CacheEvict(cacheNames = "countryCache", key = "#id")
    public Country update(Long id, String name) {
        Country country = new Country();
        country.setName(name);
        country.setId(id);
        return countryRepository.save(country);
    }

}
