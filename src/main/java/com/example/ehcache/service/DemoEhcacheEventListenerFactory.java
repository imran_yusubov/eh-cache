package com.example.ehcache.service;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;
import net.sf.ehcache.event.CacheEventListenerFactory;

import java.util.Properties;

public class DemoEhcacheEventListenerFactory extends CacheEventListenerFactory {

    @Override
    public CacheEventListener createCacheEventListener(Properties properties) {
        return new CacheEventListener() {
            @Override
            public void notifyElementRemoved(Ehcache cache, Element element) throws CacheException {

            }

            @Override
            public void notifyElementPut(Ehcache cache, Element element) throws CacheException {
                System.out.println("Added element to cache " + element);
            }

            @Override
            public void notifyElementUpdated(Ehcache cache, Element element) throws CacheException {
                System.out.println("Element updated " + element);
            }

            @Override
            public void notifyElementExpired(Ehcache cache, Element element) {

            }

            @Override
            public void notifyElementEvicted(Ehcache cache, Element element) {

            }

            @Override
            public void notifyRemoveAll(Ehcache cache) {

            }

            @Override
            public void dispose() {

            }

            @Override
            public Object clone() throws CloneNotSupportedException {
                return null;
            }
        };
    }
}
